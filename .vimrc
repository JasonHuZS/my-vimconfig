" vim: foldmethod=marker

set encoding=utf-8
set nocompatible
filetype off

" pathogen setting
execute pathogen#infect()
execute pathogen#helptags()

"set paste
set autoindent
set autowriteall
set hls
set nu
set showmatch
"set smartindent
set foldmethod=syntax
set ruler
set whichwrap=b,s,<,>,[,]
colorscheme gruvbox
set hidden
set bg=dark

set undofile
set undodir=$HOME/.vim/undo

set undolevels=1000
set undoreload=10000

set tag=./tags,./TAGS,tags,TAGS

set ts=4 sw=4
syntax enable
syntax on

filetype plugin indent on

set completeopt=longest,menu

let mapleader=","

nmap     <F8> :cn <cr>
nmap     <F7> :cp <cr>

vnoremap // y/<C-R>"<CR>
vnoremap <Leader>av y:Tabularize /<C-r>"<CR>
noremap  <leader>n <esc>:noh<cr>

" tab navigation {{{
noremap <C-t><C-n>			<esc>:tabnew<cr>
noremap <C-t><C-p>			<esc>:tabprevious<cr>
noremap <C-t><C-r>			<esc>:tabnext<cr>
noremap <C-t><C-c>			<esc>:tabclose<cr>
noremap <C-t><C-f>			<esc>:tabfirst<cr>
noremap <C-t><C-l>			<esc>:tablast<cr>
noremap <C-t><C-m>			<esc>:tabnew<cr>:call MenuDisable()<cr>
" }}}

" nerd tree and tagbar menu setup {{{
function! MenuOpen()
	if exists("t:menu_opened") && t:menu_opened == 1
		return
	endif
	if exists("t:tab_nomenu") && t:tab_nomenu == 1
		return
	endif
	NERDTree
	TagbarOpen
	setlocal winfixwidth
	wincmd =
	wincmd l
	let t:menu_opened = 1
endfunction

function! MenuClose()
	if exists("t:menu_opened") && t:menu_opened == 0
		return
	endif
	NERDTreeClose
	TagbarClose
	let t:menu_opened = 0
endfunction

function! MenuToggle()
	if !exists("t:menu_opened")
		let t:menu_opened = 0
	endif
	if t:menu_opened == 1
		call MenuClose()
	else
		call MenuOpen()
	endif
endfunction

function! MenuDisable()
	let t:tab_nomenu = 1
	call MenuClose()
endfunction

function! MenuEnable()
	let t:tab_nomenu = 0
	if t:menu_opened == 1
		call MenuOpen()
	else
		call MenuClose()
	endif
endfunction
com      MT call MenuToggle()
nnoremap MT <esc>:MT<cr>
" }}}

" let vim remeber your last edit position
" it's so wonderful obviously :)
au BufReadPost * if line("'\"") > 0|if line("'\"") <= line("$")|exe("norm '\"")|else|exe "norm $"|endif|endif

" MiniBufExplorer
let g:miniBufExplMapWindowNavVim = 1

" powerline setup {{{
set rtp+=$HOME/.local/lib/python2.7/site-packages/powerline/bindings/vim/

" Always show statusline
set laststatus=2
" }}}

" Use 256 colours (Use this setting only if your terminal supports 256 colours)
set t_Co=256

" Tagbar {{{
let g:tagbar_vertical = 25
" }}}

" Tabularize {{{
nmap <Leader>a= :Tabularize /^.\{-\}[[:alnum:][:blank:]()_]\zs=\ze[[:alnum:][:blank:]()_]<CR>
vmap <Leader>a= :Tabularize /^.\{-\}[[:alnum:][:blank:]()_]\zs=\ze[[:alnum:][:blank:]()_]<CR>
nmap <leader>a<space> :Tabularize /^\S\{-1,\}\zs \ze/l0<cr>
vmap <leader>a<space> :Tabularize /^\S\{-1,\}\zs \ze/l0<cr>
nmap <leader>a<space>= <leader>a<space><leader>a=
vmap <leader>a<space>= <leader>a<space><leader>a=
" }}}

" Syntastic {{{
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
" }}}

" Fuzzy Finder {{{
noremap <C-a><C-b>	<esc><esc>:FufBuffer<cr>
noremap <C-a><C-g>	<esc><esc>:FufFile<cr>
noremap <C-a><C-c>	<esc><esc>:FufCoverageFile<cr>
noremap <C-a><C-d>	<esc><esc>:FufDir<cr>
noremap <C-a><C-t>	<esc><esc>:FufTag<cr>
noremap <C-a><C-j>	<esc><esc>:FufJumpList<cr>
noremap <C-a><C-q>	<esc><esc>:FufQuickfix<cr>
noremap <C-a><C-l>	<esc><esc>:FufLine<cr>
noremap <C-a><C-h>	<esc><esc>:FufHelp<cr>
noremap <C-a><C-m>	<esc><esc>:FufMruFile<cr>
noremap <C-a><C-n>	<esc><esc>:FufMruCmd<cr>
noremap <C-a><C-s>	<esc><esc>:FufBookmarkFile<cr>
noremap <C-a><C-a>	<esc><esc>:FufBookmarkDir<cr>
noremap <C-a><C-r>	<esc><esc>:FufBufferTag<cr>
noremap <C-a><C-e>	<esc><esc>:FufTaggedFile<cr>
noremap <C-a><C-w>	<esc><esc>:FufChangeList<cr>
" }}}

" smartinput {{{
call smartinput#map_to_trigger('i', '*', '*', '*')
call smartinput#map_to_trigger('i', '/', '/', '/')
call smartinput#map_to_trigger('i', '-', '-', '-')
call smartinput#map_to_trigger('i', '#', '#', '#')
" { { { just to match the braces
call smartinput#map_to_trigger('i', '}', '}', '}')


function! s:rm_after_pattern(pat)
	" this function returns input string to remove all content after given
	" pattern(pat) in current line
	return '<esc>?'.a:pat.'<cr>D:noh<cr>a'
endfunction


" Vim setups {{{
call smartinput#define_rule({'at': '^\s*\%#', 'char': '"', 'input': '" ', 'filetype': ['vim']})
call smartinput#define_rule({'at': '^" \%#', 'char': '<bs>', 'input': '<bs><bs>', 'filetype': ['vim']})
" 'syntax': ['shComment', 'Comment', 'shCommentGroup', 'shQuickComment']
" }}}

" C/C++ style comments setups {{{
let s:c_style_ft=['c', 'c++', 'java', 'scala']
call smartinput#define_rule({'at': '\/\%#', 'char': '*', 'input': '* */<left><left>', 'filetype': s:c_style_ft})
call smartinput#define_rule({'at': '\/\*\s*\%#\s*\*\/', 'char': '<bs>', 'input': s:rm_after_pattern('/\*'), 'filetype': s:c_style_ft})
call smartinput#define_rule({'at': '^\s*\*\s\+\%#', 'char': '<bs>', 'input': '<esc><left>v0s<bs>', 'filetype': s:c_style_ft})
call smartinput#define_rule({'at': '^\s*\*\%#', 'char': '<bs>', 'input': '<esc>v0s<bs>', 'filetype': s:c_style_ft})
call smartinput#define_rule({'at': '^\s*\*\s*\%#\*\/', 'char': '/', 'input': '/<del><del>', 'filetype': s:c_style_ft})
call smartinput#define_rule({'at': '\*\/\%#', 'char': '<bs>', 'input': '<left><left><bs>', 'filetype': s:c_style_ft})
call smartinput#define_rule({'at': '^\s*/\%#', 'char': '/', 'input': '/ ', 'filetype': s:c_style_ft})
call smartinput#define_rule({'at': '^\/\/ \%#', 'char': '<bs>', 'input': '<bs><bs><bs>', 'filetype': s:c_style_ft})
" }}}

" # style comment setups {{{
call smartinput#define_rule({'at': '^\s*\%#', 'char': '#', 'input': '# ', 'filetype': ['python', 'sh']})
call smartinput#define_rule({'at': '^# \%#', 'char': '<bs>', 'input': '<bs><bs>', 'filetype': ['python', 'sh']})
" }}}

" Haskell setups {{{
let s:haskell_ft = ['haskell', 'lhaskell', 'ghci']
call smartinput#define_rule({'at': '^\s*-\%#', 'char': '-', 'input': '- ', 'filetype': ['haskell', 'lhaskell', 'ghci', 'cabal']})
call smartinput#define_rule({'at': '^-- \%#', 'char': '<bs>', 'input': '<bs><bs><bs>', 'filetype': ['haskell', 'lhaskell', 'ghci', 'cabal']})
call smartinput#define_rule({'at': '{\%#}', 'char': '-', 'input': '- -<left>', 'filetype': s:haskell_ft})
call smartinput#define_rule({'at': '{-\s*\%#\s*-}', 'char': '<bs>', 'input': s:rm_after_pattern('{-'), 'filetype': s:haskell_ft})
" call smartinput#define_rule({'at': '{-.*\%#.*-}', 'char': '<cr>', 'input': '<cr> - ', 'filetype': s:haskell_ft})
" call smartinput#define_rule({'at': '^\s*-.*\%#', 'char': '<cr>', 'input': '<cr> - ', 'filetype': s:haskell_ft})
call smartinput#define_rule({'at': '^\s*-\s\+\%#', 'char': '<bs>', 'input': '<esc><left>v0s<bs>', 'filetype': s:haskell_ft})
call smartinput#define_rule({'at': '^\s*-\%#', 'char': '<bs>', 'input': '<esc>v0s<bs>', 'filetype': s:haskell_ft})
call smartinput#define_rule({'at': '^\s*-\s*\%#-}', 'char': '}', 'input': '}<del><del>', 'filetype': s:haskell_ft})
call smartinput#define_rule({'at': '-}\%#', 'char': '<bs>', 'input': '<left><left><bs>', 'filetype': s:haskell_ft})
" }}}


" }}}

" some self defined helpers {{{
function! ToWindow(sig)
	python <<EOF
for w in vim.current.tabpage.windows:
	if vim.eval('a:sig') in w.buffer.name.lower():
		vim.current.window = w
		break
EOF
endfunction

noremap <C-c><C-n> <esc><esc>:call ToWindow('nerd_tree')<cr>
noremap <C-c><C-m> <esc><esc>:call ToWindow('minibufexplorer')<cr>
noremap <C-c><C-f> <esc><esc>:call ToWindow('nerd_tree')\|wincmd l<cr>
noremap <C-c><C-t> <esc><esc>:call ToWindow('tagbar')<cr>

function! CheckLeftWindow()
    python <<EOF
def examinator(b):
	s = b.name.lower()
	return b.options['buftype'] not in ('', 'help') and b.options['filetype'] != 'conque_term' # or 'nerd_tree' in s or 'tag_list' in s or 'minibufexplorer' in s

for w in vim.current.tabpage.windows:
	if not examinator(w.buffer):
		break
else:
	if len(vim.tabpages) > 1:
		vim.command('tabclose')
	else:
		vim.command('qa')
del examinator
EOF
endfunction

if has("python")
	autocmd bufenter * call CheckLeftWindow()
endif

function! SplitMotion(motion)
	python <<EOF
orig = vim.current.window
ws = [w for w in vim.current.tabpage.windows
	  if w != vim.current.window and w.buffer.options['buftype'] in ('', 'help')]
if len(ws) != 1:
	vim.command('echoerr "have no idea which window to perform given motion!"')
else:
	vim.current.window = ws[0]
	vim.command('execute "normal {}"'.format(vim.eval('a:motion')))
	vim.current.window = orig
del orig
EOF
endfunction

nnoremap <c-p><c-p>		:call SplitMotion('\<'."c-f>")<cr>
nnoremap <c-p><c-o>		:call SplitMotion('\<'."c-b>")<cr>
nnoremap <c-p><c-u>		:call SplitMotion('\<'."c-u>")<cr>
nnoremap <c-p><c-i>		:call SplitMotion('\<'."c-d>")<cr>
" }}}

"general operations {{{
map     <F2>  <C-W>w
map     <F3>  <C-W><
map     <F4>  <C-W>>
map     <F5>  <C-W>+
map     <F6>  <C-W>-
noremap <F8>  <esc>  <esc>:call ToWindow('tagbar') <cr>
noremap <F9>  <esc>  <esc>:cp                      <cr>
noremap <F10> <esc>  <esc>:cn                      <cr>

noremap <silent><C-c>			<esc><esc>
noremap <C-c><C-w>			<esc><esc>:w<cr>
noremap <C-c><C-e>			<esc><esc>:e 
noremap <C-c><C-p>			<esc><esc>:sp 
noremap <C-c><C-v>			<esc><esc>:vsp 
noremap <C-l><C-b>			<esc>:ConqueTermSplit bash_clean<cr><esc>:res 15<cr>a
noremap <C-l><C-s>			<esc>:ConqueTerm bash_clean<cr><esc>:res 15<cr>a
noremap <C-l><C-p>			<esc>:ConqueTermSplit 
noremap <C-l><C-v>			<esc>:ConqueTermVSplit 
" }}}

" prevent .vimrc getting load in some cases {{{
if &diff || exists("$GIT_DIR")
	finish
endif

if has('python')
	python import vim
	python import vimutils
	" this implement a magic to only load up local exrc if certain magic line
	" exists
	" the magic line is
	" 		" LOAD ME!
	" somewhere in the file.
	python <<EOF
import os
for f in ['.vimrc', '_vimrc']:
	if os.path.isfile(f):
		with open(f) as fd:
			for line in fd:
				if line[0] == '"' and line[1:].strip() == 'LOAD ME!':
					vim.command('set exrc')
					break
EOF
endif
" set exrc
" }}}

" launch events {{{
autocmd  vimenter * call MenuOpen()
autocmd  tabenter * call MenuOpen()
" }}}
