## My Configuration to Vim

This is my personal configuration to Vim. It mainly configures following languages:

* C
* Python
* Haskell
* Bash
* SML
* Scala

With third party and key mapping configured.

This serves as a configuration backup and reference to other Vim users.

### Dependencies

`inst-deps.sh` shall install the dependencies, including binaries and plugins, and
then reset the working directory.

note that this script can only work under debian branch with `apt-get`, for other
distributions, the script requires modification.

if `inst-deps.sh` runs successfully, Vim shall be configured. however, the script
will detect the environment and skip some configuration if they don't apply.

for example, if current machine has no ghc installed, Haskell related plugins won't
be installed.

#### Binary Dependencies

following binaries are depedencies and will be installed by inst-deps:

* git
* gmake
* gcc
* ctags
* cscope

#### Plugins

following plugins will be clone to local automatically using git:

* https://github.com/tpope/vim-pathogen
* https://github.com/oplatek/Conque-Shell
* https://github.com/scrooloose/nerdtree
* https://github.com/scrooloose/syntastic
* https://github.com/godlygeek/tabular
* https://github.com/majutsushi/tagbar
* https://github.com/tpope/vim-fugitive
* https://github.com/plasticboy/vim-markdown
* https://github.com/Shougo/vimproc.vim
* https://github.com/vim-scripts/a.vim
* https://github.com/hustmphrrr/minibufexpl.vim
* https://github.com/eagletmt/ghcmod-vim
* https://github.com/lukerandall/haskellmode-vim
* https://github.com/neovimhaskell/haskell-vim
* https://github.com/eagletmt/neco-ghc
* https://github.com/dag/vim2hs
* https://github.com/klen/python-mode
* https://github.com/jimenezrick/vimerl
* https://github.com/derekwyatt/vim-scala
* https://github.com/ktvoelker/sbt-vim
* https://github.com/mpollmeier/vim-scalaConceal
* https://github.com/tpope/vim-surround
* https://github.com/kana/vim-smartinput
* https://github.com/eparreno/vim-l9
* https://github.com/atom/fuzzy-finder
* https://github.com/morhetz/gruvbox
