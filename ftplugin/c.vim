" for ardupilot
au BufNewFile,BufRead *.pde set filetype=cpp 
setlocal ts=8 sw=8
setlocal cindent
setlocal cscopequickfix=s-,c-,d-,i-,t-,e-

" SuperTab
let g:SuperTabRetainCompletionType=2
let g:SuperTabDefaultCompletionType="<C-X><C-O>"
"
" cscope codes
cs add cscope.out
nmap <C-g>s :cs find s <C-R>=expand("<cword>")<CR><CR>
nmap <C-g>g :cs find g <C-R>=expand("<cword>")<CR><CR>
nmap <C-g>c :cs find c <C-R>=expand("<cword>")<CR><CR>
nmap <C-g>t :cs find t <C-R>=expand("<cword>")<CR><CR>
nmap <C-g>e :cs find e <C-R>=expand("<cword>")<CR><CR>
nmap <C-g>f :cs find f <C-R>=expand("<cfile>")<CR><CR>
nmap <C-g>i :cs find i <C-R>=expand("<cfile>")<CR><CR>
nmap <C-g>d :cs find d <C-R>=expand("<cword>")<CR><CR>

