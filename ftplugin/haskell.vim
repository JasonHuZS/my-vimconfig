" vim: foldmethod=marker

" environment recognition {{{
function! s:cabal_exist()
	python <<EOF
import os
import vim
def cabal_exist(): 
	vim.command('let l:ret = 0')
	path = os.path.abspath(os.getcwd())
	while len(path) > 1:
		if any(p.endswith('.cabal') for p in next(os.walk(os.getcwd()))[2]):
			vim.command('let l:ret = 1')
			break
		path = os.path.dirname(path)
cabal_exist()
del cabal_exist
EOF
	return l:ret
endfunction
" }}}

let b:has_cabal = s:cabal_exist() 
let b:ghc_staticoptions = ""
if b:has_cabal
	let b:ghc_staticoptions = "-isrc"
	setlocal path+=src
	noremap <buffer><C-h><C-m> <esc>:!cabal build<cr>
	noremap <buffer><C-h><C-n> <esc>:!cabal clean<cr>
endif
execute 'noremap <buffer><C-h><C-l> <esc>:ConqueTermSplit ghci '.b:ghc_staticoptions.'<cr><esc>:set ft=ghci<cr>a'

" Tab specific option
setlocal tabstop=8                   "A tab is 8 spaces
setlocal expandtab                   "Always uses spaces instead of tabs
setlocal softtabstop=4               "Insert 4 spaces when tab is pressed
setlocal shiftwidth=4                "An indent is 4 spaces
setlocal shiftround                  "Round indent to nearest shiftwidth multiple
setlocal autoindent

setlocal omnifunc=necoghc#omnifunc

"setlocal formatoptions+=t
"setlocal formatoptions+=c
"setlocal formatoptions+=r
"setlocal formatoptions+=o
"setlocal formatoptions+=q
setlocal textwidth=79
autocmd bufenter *.hs setlocal formatoptions+=tc


let g:syntastic_haskell_checkers = []

" general applicable key maps {{{
noremap <buffer><C-h><C-t> <Esc>:GhcModType<CR>
noremap <buffer><C-h><C-r> <Esc>:GhcModTypeClear<CR>
noremap <buffer><C-h><C-c> <Esc>:w<CR>:GhcModCheck<CR>
noremap <buffer><C-h><C-e> <Esc>:GhcModExpand<CR>
noremap <buffer><C-h><C-d> <Esc>:GHCReload<CR>
map     <buffer><C-h><C-i> <Esc>:w<CR>^:GHCReload<CR>_T
noremap <buffer><C-h><C-x> <Esc>:GHCi
nmap    <buffer><leader>a: :Tabularize /::<cr>
vmap    <buffer><leader>a: :Tabularize /::<cr>
nmap    <buffer><leader>a- :Tabularize /-><cr>
vmap    <buffer><leader>a- :Tabularize /-><cr>
nmap    <buffer><leader>a\| :Tabularize /\|<cr>
vmap    <buffer><leader>a\| :Tabularize /\|<cr>
" }}}

" vim2hs settings
let g:haskell_conceal_wide = 1
" let g:haskell_conceal_enumerations = 0

" haskellmode-vim settings
au BufEnter *.hs compiler ghc
let g:haddock_browser                = "/usr/bin/iceweasel"
let g:haddock_docdir                 = "/home/hu/Tools/ghc/share/doc/ghc/html/"
let g:ghc                            = "/home/hu/Tools/ghc/bin/ghc"
" turn off haskellmode completion
let g:haskellmode_completion_ghc     = 0
let g:haskellmode_completion_haddock = 0

" from Haskell wiki page {{{
" https://wiki.haskell.org/Vim
let s:width = 80

function! HaskellModuleSection(...)
    let name = 0 < a:0 ? a:1 : inputdialog("Section name: ")

    return  repeat('-', s:width) . "\n"
    \       . "--  " . name . "\n"
    \       . "\n"
endfunction

nmap <buffer><silent> --s "=HaskellModuleSection()<CR>gp

function! HaskellModuleHeader(...)
    let name = 0 < a:0 ? a:1 : inputdialog("Module: ")
    let note = 1 < a:0 ? a:2 : inputdialog("Note: ")
    let description = 2 < a:0 ? a:3 : inputdialog("Describe this module: ")
    
    return  repeat('-', s:width) . "\n" 
    \       . "-- | \n" 
    \       . "-- Module      : " . name . "\n"
    \       . "-- Note        : " . note . "\n"
    \       . "-- \n"
    \       . "-- " . description . "\n"
    \       . "-- \n"
    \       . repeat('-', s:width) . "\n"
    \       . "\n"
endfunction

nmap <buffer><silent> --h "=HaskellModuleHeader()<CR>:0put =<CR>
" }}}

" haskell.vim config {{{
" let g:haskell_enable_quantification   = 1 " to enable highlighting of forall
" let g:haskell_enable_recursivedo      = 1 " to enable highlighting of mdo and rec
" let g:haskell_enable_arrowsyntax      = 1 " to enable highlighting of proc
" let g:haskell_enable_pattern_synonyms = 1 " to enable highlighting of pattern
" let g:haskell_enable_typeroles        = 1 " to enable highlighting of type roles
" let g:haskell_enable_static_pointers  = 1 " to enable highlighting of static
let g:haskell_indent_if               = 3
let g:haskell_indent_case             = 2
let g:haskell_indent_let              = 1
let g:haskell_indent_where            = 6
let g:haskell_indent_do               = 3
let g:haskell_indent_in               = 1
" }}}
