setlocal ts=4 sw=4 et
setlocal tw=79
setlocal formatoptions+=t
setlocal formatoptions+=c
setlocal formatoptions+=r
setlocal formatoptions+=o

let g:pymode_rope_completion_bind = '<C-Space>'
let g:pymode_rope_autoimport      = 1

nmap    <buffer><Leader>a: :Tabularize /:\zs<CR>
vmap    <buffer><Leader>a: :Tabularize /:\zs<CR>
noremap <buffer><C-p><C-l> <esc>:ConqueTermSplit ipython<cr><esc>:set ft=python<cr>:res 15<cr>a
noremap <buffer><C-p><C-f> <esc>:PymodeLintAuto<cr>
noremap <buffer><C-p><C-c> <esc>:PymodeLint<cr>
