set et
set ts=2
set sw=2

noremap <buffer><C-s><C-l> <esc>:ConqueTermSplit sbt<cr><esc>:res 20<cr>a
