#!/bin/bash
# vim: foldmethod=marker
# the directory this folder is running in
WD="`dirname $(readlink -f "${BASH_SOURCE[0]}")`"
INSTALL="apt-get install -y"

mkdir -p ~/.vim/bundle
mkdir -p ~/.vim/autoload

# get all the dependencies to local folder
if ! hash git; then
	sudo $INSTALL git-core
fi
if ! hash gcc; then
	sudo $INSTALL gcc
fi
if ! hash make; then
	sudo $INSTALL build-essential
fi
if ! hash ctags; then
	sudo $INSTALL ctags
fi
if ! hash cscope; then
	sudo $INSTALL cscope
fi
git clone https://github.com/tpope/vim-pathogen
mv vim-pathogen/autoload/pathogen.vim ~/.vim/autoload
rm -rf vim-pathogen

# bundle setup {{{
mkdir bundle
cd bundle
git clone https://github.com/oplatek/Conque-Shell &
git clone https://github.com/scrooloose/nerdtree &
git clone https://github.com/scrooloose/syntastic &
git clone https://github.com/godlygeek/tabular &
wait
git clone https://github.com/majutsushi/tagbar &
git clone https://github.com/tpope/vim-fugitive &
git clone https://github.com/plasticboy/vim-markdown &
(
	git clone https://github.com/Shougo/vimproc.vim
	cd vimproc.vim
	make
) &
wait
git clone https://github.com/HuStmpHrrr/pythonutils-vim &
git clone https://github.com/tpope/vim-surround &
git clone https://github.com/kana/vim-smartinput &
git clone https://github.com/eparreno/vim-l9 &
wait
git clone https://github.com/atom/fuzzy-finder &
git clone https://github.com/vim-scripts/a.vim &
git clone https://github.com/hustmphrrr/minibufexpl.vim &
git clone https://github.com/morhetz/gruvbox &
wait
if hash ghc; then
	git clone https://github.com/eagletmt/ghcmod-vim &
	git clone https://github.com/lukerandall/haskellmode-vim &
	git clone https://github.com/eagletmt/neco-ghc &
	git clone https://github.com/neovimhaskell/haskell-vim &
	git clone https://github.com/dag/vim2hs &
	wait
fi
if hash python; then
	git clone https://github.com/klen/python-mode
fi
if hash erl; then
	git clone https://github.com/jimenezrick/vimerl
fi
if hash sbt; then
	git clone https://github.com/derekwyatt/vim-scala &
	git clone https://github.com/ktvoelker/sbt-vim &
	git clone https://github.com/mpollmeier/vim-scalaConceal &
	wait
fi
mv -f * ~/.vim/bundle
# }}}

cd "$WD"
cp .vimrc ~
cp -rf ftplugin ~/.vim

if ! python -c 'import powerline' 2>/dev/null; then
	sed -i 's/^.*powerline/" &/' ~/.vimrc
fi

git clean -fdx
